package developer.javid.ir.tableview;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public class DataSourceImpl implements TableDataSource<String, String, String, String> {
    private final Context mContext;
    private List<CellData> listRowNum;
    private int mRowsCount;
    private int mColumnsCount;

    public DataSourceImpl(Context context, List<CellData> csvFileUri) {
        mContext = context;
        listRowNum = csvFileUri;
        init();
    }

    public List<CellData> getListRowNum() {
        return listRowNum;
    }

    public void setListRowNum(List<CellData> listRowNum) {
        this.listRowNum = listRowNum;
    }

    @Override
    public int getRowsCount() {
        return mRowsCount;
    }

    @Override
    public int getColumnsCount() {
        return mColumnsCount;
    }

    @Override
    public String getFirstHeaderData() {
        return getItemData(0, 0);
    }

    @Override
    public String getRowHeaderData(int index) {
        return getItemData(index, 0);
    }

    @Override
    public String getColumnHeaderData(int index) {
        return getItemData(0, index);
    }

    @Override
    public String getItemData(int rowIndex, int columnIndex) {
        try {
            List<String> rowList = getRow(rowIndex);
            return rowList == null ? "" : rowList.get(columnIndex);
        } catch (Exception e) {
            return null;
        }
    }

    void init() {
        mRowsCount = listRowNum.size();
        mColumnsCount = getRow(0).size();
    }

    private List<String> getRow(int rowIndex) {
        List<String> result = new ArrayList<>();
//        List<CellData> cachedRow = TableManager.getInstance().getData(rowIndex);
        List<CellData> cachedRow = getListRowNum();
        if (cachedRow != null && !cachedRow.isEmpty()) {
            for (int count = cachedRow.size(), i = 0; i < count; i++) {
                String cachedItem = cachedRow.get(i).getTitle();
                result.add(cachedItem);
            }
        }
        return result;
    }
}