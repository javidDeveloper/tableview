package developer.javid.ir.tableview;

import java.util.ArrayList;
import java.util.List;

import developer.javid.ir.tableview.search.array_search.Searchable;

/**
 * Developed by javid
 * Project : tableView
 */
public class CellData implements Searchable {
    private String title;

    public CellData(String title) {
        this.title = title;
    }

    public CellData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getSearchableContent() {
        return getTitle();
    }

    @Override
    public List<String> getSortPriorities() {
        return new ArrayList<>();
    }
}

