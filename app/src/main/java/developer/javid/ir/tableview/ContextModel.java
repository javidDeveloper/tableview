package developer.javid.ir.tableview;

import android.app.Activity;
import android.content.Context;

/**
 * Developed by javid
 * Project : tableView
 */
public class ContextModel {
    public static  Context  mContext;
    public static Activity activity;

    public ContextModel() {
    }

    public static Context getmContext() {
        return mContext;
    }

    public static void setmContext(Context mContext) {
        ContextModel.mContext = mContext;
    }

    public static Activity getActivity() {
        return activity;
    }

    public static void setActivity(Activity activity) {
        ContextModel.activity = activity;
    }
}
