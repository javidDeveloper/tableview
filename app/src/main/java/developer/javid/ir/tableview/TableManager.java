package developer.javid.ir.tableview;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Developed by javid
 * Project : tableView
 */
public class TableManager {
    private static TableManager instance;
    private List<CellData> data = new ArrayList<>();
    private List<String> rowNums = new ArrayList<>();
    private CellData cellData;
    private Map<Integer, List<CellData>> dataMap = new ConcurrentHashMap<>();

    public static TableManager getInstance() {
        if (instance == null)
            instance = new TableManager();
        return instance;
    }


    public Map<Integer, List<CellData>> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<Integer, List<CellData>> dataMap) {
        this.dataMap = dataMap;
    }

    public List<CellData> getData(int row) {
        return dataMap.get(row);
    }

    public TableManager() {
        if(!dataMap.isEmpty())
            return ;

        for (int i = 0; i <= 20; i++) {
            data = new ArrayList<>();
            if (i == 0) {
                cellData = new CellData("نماد");data.add(cellData);
                cellData = new CellData("a");data.add(cellData);
                cellData = new CellData("b");data.add(cellData);
                cellData = new CellData("c");data.add(cellData);
                cellData = new CellData("d");data.add(cellData);
                cellData = new CellData("e");data.add(cellData);
                cellData = new CellData("f");data.add(cellData);
                cellData = new CellData("g");data.add(cellData);
                cellData = new CellData("h");data.add(cellData);
                cellData = new CellData("i");data.add(cellData);
                cellData = new CellData("j");data.add(cellData);
                cellData = new CellData("k");data.add(cellData);
                cellData = new CellData("l");data.add(cellData);
                cellData = new CellData("m");data.add(cellData);
                cellData = new CellData("n");data.add(cellData);
                cellData = new CellData("o");data.add(cellData);
                cellData = new CellData("p");data.add(cellData);
                cellData = new CellData("q");data.add(cellData);
                cellData = new CellData("r");data.add(cellData);
                cellData = new CellData("s");data.add(cellData);
                cellData = new CellData("t");data.add(cellData);
                cellData = new CellData("t");data.add(cellData);
                cellData = new CellData("u");data.add(cellData);
                cellData = new CellData("v");data.add(cellData);
                cellData = new CellData("w");data.add(cellData);
                cellData = new CellData("x");data.add(cellData);
                cellData = new CellData("y");data.add(cellData);
                cellData = new CellData("z");data.add(cellData);
                dataMap.put(i, data);

            }
             if (i == 1) {
                
                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
             if (i == 2) {

                 cellData = new CellData(i + "");data.add(cellData);
                 cellData = new CellData("a" + "_" + i);data.add(cellData);
                 cellData = new CellData("b" + "_" + i);data.add(cellData);
                 cellData = new CellData("c" + "_" + i);data.add(cellData);
                 cellData = new CellData("d" + "_" + i);data.add(cellData);
                 cellData = new CellData("e" + "_" + i);data.add(cellData);
                 cellData = new CellData("f" + "_" + i);data.add(cellData);
                 cellData = new CellData("g" + "_" + i);data.add(cellData);
                 cellData = new CellData("h" + "_" + i);data.add(cellData);
                 cellData = new CellData("i" + "_" + i);data.add(cellData);
                 cellData = new CellData("j" + "_" + i);data.add(cellData);
                 cellData = new CellData("k" + "_" + i);data.add(cellData);
                 cellData = new CellData("l" + "_" + i);data.add(cellData);
                 cellData = new CellData("m" + "_" + i);data.add(cellData);
                 cellData = new CellData("n" + "_" + i);data.add(cellData);
                 cellData = new CellData("o" + "_" + i);data.add(cellData);
                 cellData = new CellData("p" + "_" + i);data.add(cellData);
                 cellData = new CellData("q" + "_" + i);data.add(cellData);
                 cellData = new CellData("r" + "_" + i);data.add(cellData);
                 cellData = new CellData("s" + "_" + i);data.add(cellData);
                 cellData = new CellData("t" + "_" + i);data.add(cellData);
                 cellData = new CellData("t" + "_" + i);data.add(cellData);
                 cellData = new CellData("u" + "_" + i);data.add(cellData);
                 cellData = new CellData("v" + "_" + i);data.add(cellData);
                 cellData = new CellData("w" + "_" + i);data.add(cellData);
                 cellData = new CellData("x" + "_" + i);data.add(cellData);
                 cellData = new CellData("y" + "_" + i);data.add(cellData);
                 cellData = new CellData("z" + "_" + i);data.add(cellData);
                 dataMap.put(i, data);
            }
             if (i == 3) {

                 cellData = new CellData(i + "");data.add(cellData);
                 cellData = new CellData("a" + "_" + i);data.add(cellData);
                 cellData = new CellData("b" + "_" + i);data.add(cellData);
                 cellData = new CellData("c" + "_" + i);data.add(cellData);
                 cellData = new CellData("d" + "_" + i);data.add(cellData);
                 cellData = new CellData("e" + "_" + i);data.add(cellData);
                 cellData = new CellData("f" + "_" + i);data.add(cellData);
                 cellData = new CellData("g" + "_" + i);data.add(cellData);
                 cellData = new CellData("h" + "_" + i);data.add(cellData);
                 cellData = new CellData("i" + "_" + i);data.add(cellData);
                 cellData = new CellData("j" + "_" + i);data.add(cellData);
                 cellData = new CellData("k" + "_" + i);data.add(cellData);
                 cellData = new CellData("l" + "_" + i);data.add(cellData);
                 cellData = new CellData("m" + "_" + i);data.add(cellData);
                 cellData = new CellData("n" + "_" + i);data.add(cellData);
                 cellData = new CellData("o" + "_" + i);data.add(cellData);
                 cellData = new CellData("p" + "_" + i);data.add(cellData);
                 cellData = new CellData("q" + "_" + i);data.add(cellData);
                 cellData = new CellData("r" + "_" + i);data.add(cellData);
                 cellData = new CellData("s" + "_" + i);data.add(cellData);
                 cellData = new CellData("t" + "_" + i);data.add(cellData);
                 cellData = new CellData("t" + "_" + i);data.add(cellData);
                 cellData = new CellData("u" + "_" + i);data.add(cellData);
                 cellData = new CellData("v" + "_" + i);data.add(cellData);
                 cellData = new CellData("w" + "_" + i);data.add(cellData);
                 cellData = new CellData("x" + "_" + i);data.add(cellData);
                 cellData = new CellData("y" + "_" + i);data.add(cellData);
                 cellData = new CellData("z" + "_" + i);data.add(cellData);
                 dataMap.put(i, data);
            }
             if (i == 4) {

                 cellData = new CellData(i + "");data.add(cellData);
                 cellData = new CellData("a" + "_" + i);data.add(cellData);
                 cellData = new CellData("b" + "_" + i);data.add(cellData);
                 cellData = new CellData("c" + "_" + i);data.add(cellData);
                 cellData = new CellData("d" + "_" + i);data.add(cellData);
                 cellData = new CellData("e" + "_" + i);data.add(cellData);
                 cellData = new CellData("f" + "_" + i);data.add(cellData);
                 cellData = new CellData("g" + "_" + i);data.add(cellData);
                 cellData = new CellData("h" + "_" + i);data.add(cellData);
                 cellData = new CellData("i" + "_" + i);data.add(cellData);
                 cellData = new CellData("j" + "_" + i);data.add(cellData);
                 cellData = new CellData("k" + "_" + i);data.add(cellData);
                 cellData = new CellData("l" + "_" + i);data.add(cellData);
                 cellData = new CellData("m" + "_" + i);data.add(cellData);
                 cellData = new CellData("n" + "_" + i);data.add(cellData);
                 cellData = new CellData("o" + "_" + i);data.add(cellData);
                 cellData = new CellData("p" + "_" + i);data.add(cellData);
                 cellData = new CellData("q" + "_" + i);data.add(cellData);
                 cellData = new CellData("r" + "_" + i);data.add(cellData);
                 cellData = new CellData("s" + "_" + i);data.add(cellData);
                 cellData = new CellData("t" + "_" + i);data.add(cellData);
                 cellData = new CellData("t" + "_" + i);data.add(cellData);
                 cellData = new CellData("u" + "_" + i);data.add(cellData);
                 cellData = new CellData("v" + "_" + i);data.add(cellData);
                 cellData = new CellData("w" + "_" + i);data.add(cellData);
                 cellData = new CellData("x" + "_" + i);data.add(cellData);
                 cellData = new CellData("y" + "_" + i);data.add(cellData);
                 cellData = new CellData("z" + "_" + i);data.add(cellData);
                 dataMap.put(i, data);
            }
            if (i == 5)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 6)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 7)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 8)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 9)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 10)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 11)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 12)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 13)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 14)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 15)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 16)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 17)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 18)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 19)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }
            if (i == 20)  {

                cellData = new CellData(i + "");data.add(cellData);
                cellData = new CellData("a" + "_" + i);data.add(cellData);
                cellData = new CellData("b" + "_" + i);data.add(cellData);
                cellData = new CellData("c" + "_" + i);data.add(cellData);
                cellData = new CellData("d" + "_" + i);data.add(cellData);
                cellData = new CellData("e" + "_" + i);data.add(cellData);
                cellData = new CellData("f" + "_" + i);data.add(cellData);
                cellData = new CellData("g" + "_" + i);data.add(cellData);
                cellData = new CellData("h" + "_" + i);data.add(cellData);
                cellData = new CellData("i" + "_" + i);data.add(cellData);
                cellData = new CellData("j" + "_" + i);data.add(cellData);
                cellData = new CellData("k" + "_" + i);data.add(cellData);
                cellData = new CellData("l" + "_" + i);data.add(cellData);
                cellData = new CellData("m" + "_" + i);data.add(cellData);
                cellData = new CellData("n" + "_" + i);data.add(cellData);
                cellData = new CellData("o" + "_" + i);data.add(cellData);
                cellData = new CellData("p" + "_" + i);data.add(cellData);
                cellData = new CellData("q" + "_" + i);data.add(cellData);
                cellData = new CellData("r" + "_" + i);data.add(cellData);
                cellData = new CellData("s" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("t" + "_" + i);data.add(cellData);
                cellData = new CellData("u" + "_" + i);data.add(cellData);
                cellData = new CellData("v" + "_" + i);data.add(cellData);
                cellData = new CellData("w" + "_" + i);data.add(cellData);
                cellData = new CellData("x" + "_" + i);data.add(cellData);
                cellData = new CellData("y" + "_" + i);data.add(cellData);
                cellData = new CellData("z" + "_" + i);data.add(cellData);
                dataMap.put(i, data);
            }

        }
    }
}
