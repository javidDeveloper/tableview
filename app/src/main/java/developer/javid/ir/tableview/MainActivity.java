package developer.javid.ir.tableview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.cleveroad.adaptivetablelayout.AdaptiveTableLayout;
import com.cleveroad.adaptivetablelayout.LinkedAdaptiveTableAdapter;
import com.cleveroad.adaptivetablelayout.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import developer.javid.ir.tableview.search.OnFinishIndexing;
import developer.javid.ir.tableview.search.array_search.ArraySearch;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {
    private Context mContext = MainActivity.this;
    private SampleLinkedTableAdapter mTableAdapter;
    private AdaptiveTableLayout mTableLayout;
    private DataSourceImpl mCsvFileDataSource;
    private List<CellData> cellDatas= new ArrayList<>();
    private Map<Integer, List<CellData>> cellDataMap = new ConcurrentHashMap<>();
    private EditText editSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ContextModel.setActivity(this);
        cellDataMap = TableManager.getInstance().getDataMap();
        for (List<CellData> list : cellDataMap.values()) {
            cellDatas.addAll(list);
        }
        mTableLayout = findViewById(R.id.tableLayout);
        editSearch = findViewById(R.id.edit_search);
        initArraySearch();
        initAdapter();
    }

    private void initArraySearch() {
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                final ArraySearch<CellData> arraySearch = new ArraySearch<>();
                arraySearch.setItems(cellDatas, new OnFinishIndexing() {
                    @Override
                    public void onFinish() {

                    }
                });
                arraySearch.setCurrentSearchText(s.toString() + "");
                arraySearch.getSearchedItems();
                mCsvFileDataSource.setListRowNum(arraySearch.getSearchedItems());
                mTableAdapter.notifyDataSetChanged();
            }
        });
    }

    private void initAdapter() {
        mCsvFileDataSource = new DataSourceImpl(mContext, cellDatas);
        mTableAdapter = new SampleLinkedTableAdapter(mContext, mCsvFileDataSource);
        mTableAdapter.setOnItemClickListener(this);
        mTableLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        mTableLayout.setAdapter(mTableAdapter);

    }

    @Override
    public void onItemClick(int row, int column) {
        Toast.makeText(mContext, TableManager.getInstance().getData(row).get(column).getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRowHeaderClick(int row) {

    }

    @Override
    public void onColumnHeaderClick(int column) {

    }

    @Override
    public void onLeftTopHeaderClick() {

    }
}
