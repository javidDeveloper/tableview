package developer.javid.ir.tableview.search;

/**
 * Created by mansour on 2/18/18.
 */

public interface OnFinishIndexing {
    public void onFinish();
}
