package developer.javid.ir.tableview.search.array_search;

import java.util.List;

/**
 * Created by mansour on 2/18/18.
 */

public interface Searchable {
    public String getSearchableContent();
    public List<String> getSortPriorities();
}
